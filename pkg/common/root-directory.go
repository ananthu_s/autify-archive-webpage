package common

import (
	"log"
	"os"
	"path"
)

func RootDir() string {
	fileName, err := os.Executable()

	if err != nil {
		log.Fatal("cannot find the correct directory")
	}

	return path.Dir(fileName)
}
