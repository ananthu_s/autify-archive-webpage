package localise

import (
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
)

func downloadAsset(payload *SelectionPayload, hashWithExtension, relativeUrl string) error {
	assetLocalUrl := path.Join(payload.folderPath, payload.pageUrl.Host, payload.pageUrl.Path, payload.pageUrl.RawQuery, hashWithExtension)
	absoluteAssetUrl, err := url.JoinPath(payload.pageUrl.Scheme+"://", payload.pageUrl.Host, relativeUrl)

	if err != nil {
		return err
	}

	response, err := http.Get(absoluteAssetUrl)

	if err != nil {
		return err
	}

	defer func(Body io.ReadCloser) {
		innerErr := Body.Close()
		if innerErr != nil {
			log.Println("error closing http body:", absoluteAssetUrl)
		}
	}(response.Body)

	if err = os.MkdirAll(filepath.Dir(assetLocalUrl), os.ModePerm); err != nil {
		return err
	}

	file, err := os.Create(assetLocalUrl)
	if err != nil {
		return err
	}

	if _, err = io.Copy(file, response.Body); err != nil {
		return err
	}

	*payload.linksCount += 1

	if payload.isImage {
		*payload.imagesCount += 1
	}
	return nil
}
