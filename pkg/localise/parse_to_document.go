package localise

import (
	"github.com/PuerkitoBio/goquery"
	"os"
)

func parseToDocument(filePath string) (document *goquery.Document, err error) {
	htmlFile, err := os.OpenFile(filePath, os.O_RDWR, 0755)

	if err != nil {
		return
	}

	document, err = goquery.NewDocumentFromReader(htmlFile)

	if err != nil {
		return
	}
	return
}
