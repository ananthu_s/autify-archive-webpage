package download

import (
	"io"
	"log"
	"net/http"
	"os"
)

func ToFile(downloadUrl, filePath string) (err error) {
	resp, err := http.Get(downloadUrl)

	if err != nil {
		return
	}

	defer func() {
		innerError := resp.Body.Close()
		if innerError != nil {
			log.Printf("Could not close body for: %s", downloadUrl)
			return
		}
	}()

	if err != nil {
		log.Printf("Could not download url for: %s", downloadUrl)
		return
	}

	file, err := os.Create(filePath)

	if err != nil {
		log.Printf("Could not create file %s, for: %s", filePath, downloadUrl)
		return
	}

	_, err = io.Copy(file, resp.Body)

	if err != nil {
		log.Printf("Could not save file %s, for: %s", filePath, downloadUrl)
		return
	}
	return
}
