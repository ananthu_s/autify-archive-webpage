package common

import (
	"regexp"
	"testing"
)

func TestRelativeToAbsolutePath(t *testing.T) {
	type args struct {
		givenPath string
	}
	tests := []struct {
		name string
		args args
		want *regexp.Regexp
	}{
		{
			name: "should append relative paths",
			args: args{
				givenPath: "./root",
			},
			want: regexp.MustCompile(`^\/.+\/root$`),
		},
		{
			name: "should return the same absolute paths",
			args: args{
				givenPath: "/root",
			},
			want: regexp.MustCompile(`^/root$`),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RelativeToAbsolutePath(tt.args.givenPath); !tt.want.MatchString(got) {
				t.Errorf("failed to match pattern, %s, %s", got, tt.want.String())
			}
		})
	}
}
