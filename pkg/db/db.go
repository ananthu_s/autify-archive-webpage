package db

import (
	"fmt"
	"github.com/tidwall/buntdb"
	"log"
	"net/url"
	"time"
)

// UpdateUrl
// Sets the zero valued entries for a given url
func UpdateUrl(db *buntdb.DB, source *url.URL, numLinks, images string, time time.Time) error {
	err := db.Update(func(tx *buntdb.Tx) error {
		escapedSource := url.QueryEscape(source.String())
		_, _, err := tx.Set(fmt.Sprintf("%s:num_links", escapedSource), numLinks, nil)
		_, _, err = tx.Set(fmt.Sprintf("%s:images", escapedSource), images, nil)
		_, _, err = tx.Set(fmt.Sprintf("%s:last_fetch", escapedSource), time.String(), nil)
		return err
	})

	if err != nil {
		log.Fatal("url initialization failed", err)
	}
	return err
}

func GetUrlMetadata(db *buntdb.DB, source string) (numLinks, imageLinks, lastFetch string, err error) {
	err = db.View(func(tx *buntdb.Tx) error {
		escapedSource := url.QueryEscape(source)

		numLinks, err = tx.Get(escapedSource + ":num_links")
		imageLinks, err = tx.Get(escapedSource + ":images")
		lastFetch, err = tx.Get(escapedSource + ":last_fetch")
		return err
	})
	return
}
