package common

import (
	"hash/fnv"
	"log"
	"strconv"
)

// Hash
// Hashes urls to number string for archival.
func Hash(url string) string {
	hash := fnv.New64a()
	_, err := hash.Write([]byte(url))

	if err != nil {
		log.Fatal("Hashing not working")
	}

	return strconv.FormatUint(hash.Sum64(), 10)
}
