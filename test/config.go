package test

import (
	"archive-webpage/config"
	"github.com/urfave/cli/v2"
)

// DownloadFolderTest
// Duplicates default config and changes `Value`
func DownloadFolderTest() cli.StringFlag {
	defaultConfig := config.DownloadFolder
	defaultConfig.Value = GetTempDirectory()
	return defaultConfig
}

// ArchiveFolderTest
// Duplicates default config and changes `Value`
func ArchiveFolderTest() cli.StringFlag {
	defaultConfig := cli.StringFlag(config.ArchiveFolder)
	defaultConfig.Value = GetTempDirectory()
	return defaultConfig
}
