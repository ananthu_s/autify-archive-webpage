package parse

import "net/url"

func Urls(stringUrls ...string) (parsedUrls []*url.URL, failedUrls []string) {
	for _, stringUrl := range stringUrls {
		parsedUrl, err := url.ParseRequestURI(stringUrl)

		if err != nil {
			failedUrls = append(failedUrls, stringUrl)
			continue
		}
		parsedUrls = append(parsedUrls, parsedUrl)
	}
	return
}
