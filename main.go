package main

import (
	"archive-webpage/config"
	"archive-webpage/internal/runners"
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

func main() {
	app := &cli.App{
		Action: runners.Default,
		Commands: []*cli.Command{
			{
				Name:    "metadata",
				Aliases: []string{"m"},
				Usage:   "pints out the details of the given url",
				Action:  runners.Metadata,
				Flags: []cli.Flag{
					&config.ArchiveFolder,
					&config.DownloadFolder,
				},
			},
		},
		Flags: []cli.Flag{
			&config.DownloadFolder,
			&config.ArchiveFolder,
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
