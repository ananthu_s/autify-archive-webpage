package common

import (
	"path"
	"strings"
)

func RelativeToAbsolutePath(givenPath string) string {
	// then it's relative
	if !strings.HasPrefix(givenPath, "/") {

		return path.Join(RootDir(), givenPath)
	}
	return givenPath
}
