package download

import (
	"archive-webpage/test"
	"os"
	"testing"
)

func TestToFile(t *testing.T) {
	destination := test.GetTempFilePath("index.html")

	type args struct {
		downloadUrl string
		filePath    string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should download html",
			args: args{
				downloadUrl: "https://autify.com/",
				filePath:    destination,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ToFile(tt.args.downloadUrl, tt.args.filePath); (err != nil) != tt.wantErr {
				t.Errorf("ToFile() error = %v, wantErr %v", err, tt.wantErr)

				if _, statErr := os.Stat(destination); statErr != nil {
					t.Error("File not downloaded")
				}
			}
		})
		t.Cleanup(func() {
			_ = os.RemoveAll(destination)
		})
	}
}
