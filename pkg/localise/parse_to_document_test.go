package localise

import (
	"archive-webpage/test"
	"github.com/PuerkitoBio/goquery"
	"os"
	"reflect"
	"testing"
)

func Test_parseToDocument(t *testing.T) {
	file, _ := os.OpenFile(test.GetTestDirectory("empty.html"), os.O_RDONLY, os.ModePerm)
	expectedHtml, _ := goquery.NewDocumentFromReader(file)

	type args struct {
		filePath string
	}
	tests := []struct {
		name         string
		args         args
		wantDocument *goquery.Document
		wantErr      bool
	}{{
		name:         "should parse correctly simple html file",
		args:         args{filePath: test.GetTestDirectory("empty.html")},
		wantDocument: expectedHtml,
		wantErr:      false,
	},
		{
			name: "should fail when invalid file",
			args: args{
				filePath: test.GetTestDirectory("invalidfile"),
			},
			wantDocument: nil,
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDocument, err := parseToDocument(tt.args.filePath)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseToDocument() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(&gotDocument, &tt.wantDocument) {
				t.Errorf("parseToDocument() gotDocument = %v, want %v", gotDocument, tt.wantDocument)
			}
		})
	}
}
