package name

import "testing"

func TestAppendExtension(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "should add `html`, when not found", args: args{
			name: "test",
		}, want: "test.html"},

		{name: "should add `html`, when it's a file", args: args{
			name: "test.mp3",
		}, want: "test.mp3.html"},

		{name: "should not add `html`, when it already has `html` prefix", args: args{
			name: "test.html",
		}, want: "test.html"},

		{name: "should add `l`, when it `htm` prefix exists", args: args{
			name: "test.htm",
		}, want: "test.html"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppendExtension(tt.args.name); got != tt.want {
				t.Errorf("AppendExtension() = %v, want %v", got, tt.want)
			}
		})
	}
}
