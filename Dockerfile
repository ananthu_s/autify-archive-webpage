FROM golang as builder

WORKDIR /app

COPY . .

RUN go mod download

RUN go build


FROM debian AS runner

WORKDIR /

COPY --from=builder  /app/archive-webpage /usr/local/bin/archive-webpage

VOLUME /data
WORKDIR /data

RUN apt-get update
RUN apt-get install ca-certificates -y
RUN update-ca-certificates

ENTRYPOINT ["archive-webpage"]