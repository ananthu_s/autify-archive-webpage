package name

import (
	"net/url"
	"testing"
)

func TestUrl(t *testing.T) {
	simplePath, _ := url.Parse("https://stackoverflow.com")
	onePath, _ := url.Parse("https://stackoverflow.com/search?q=golang&s=934b6580-b01d-40b1-961b-c39562a035fc")
	twoPath, _ := url.Parse("https://stackoverflow.com/advanced/search?q=golang&s=934b6580-b01d-40b1-961b-c39562a035fc")

	type args struct {
		link *url.URL
	}
	tests := []struct {
		name         string
		args         args
		wantFileName string
		wantFolder   string
	}{
		{name: "no path query", args: args{link: simplePath}, wantFileName: "stackoverflow.com", wantFolder: ""},
		{name: "single path query", args: args{link: onePath}, wantFileName: "search?q=golang&s=934b6580-b01d-40b1-961b-c39562a035fc", wantFolder: "stackoverflow.com"},
		{name: "two path query", args: args{link: twoPath}, wantFileName: "search?q=golang&s=934b6580-b01d-40b1-961b-c39562a035fc", wantFolder: "stackoverflow.com/advanced"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFileName, gotFolder := FromUrl(tt.args.link)
			if gotFileName != tt.wantFileName {
				t.Errorf("FromUrl() gotFileName = %v, want %v", gotFileName, tt.wantFileName)
			}
			if gotFolder != tt.wantFolder {
				t.Errorf("FromUrl() gotFolder = %v, want %v", gotFolder, tt.wantFolder)
			}
		})
	}
}
