package config

import (
	"archive-webpage/pkg/common"
	"github.com/urfave/cli/v2"
	"path"
)

const DownloadFolderFlag = "download-folder"
const ArchiveFolderFlag = "archive-folder"

var DownloadFolderPath string
var ArchiveFolderPath string

var DownloadFolder = cli.StringFlag{
	Name:        DownloadFolderFlag,
	Aliases:     []string{"d"},
	Value:       common.RootDir(),
	Usage:       "Path to where the html's should be downloaded.",
	Destination: &DownloadFolderPath,
	DefaultText: common.RootDir(),
}

var ArchiveFolder = cli.StringFlag{
	Name:        ArchiveFolderFlag,
	Aliases:     []string{"a"},
	Value:       path.Join(common.RootDir(), "archive"),
	Usage:       "Path to where the local archive should be downloaded.",
	Destination: &ArchiveFolderPath,
	DefaultText: path.Join(common.RootDir(), "archive"),
}
