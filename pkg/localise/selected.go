package localise

import (
	"archive-webpage/pkg/common"
	"log"
	"path/filepath"
	"strings"
)

func Selected(payload *SelectionPayload) {
	defer payload.pageWg.Done()
	// relative url
	src, hasSrc := payload.selected.Attr(payload.attribute)

	if !hasSrc {
		log.Printf("`%s` not found for %s", payload.attribute, payload.selected.Nodes[0].Data)
		return
	}

	if strings.HasPrefix(src, "data:") {
		return
	}

	hashName := common.Hash(src)
	_, alreadyProcessed := (*payload.pageUrlCache).Load(hashName)

	// saves to hash
	if !alreadyProcessed {
		extension := filepath.Ext(src)
		hashWithExtension := hashName + extension

		if extension == "" {
			log.Println("empty extension")
			return
		}

		(*payload.pageUrlCache).Store(hashName, hashWithExtension)
		err := downloadAsset(payload, hashWithExtension, src)
		if err != nil {
			// TODO: handle error correctly
			log.Println(err.Error())
			return
		}
		// update attribute
		payload.selected.SetAttr(payload.attribute, hashWithExtension)
	}
}
