package name

import "strings"

// AppendExtension will add `.html` to the end.
// Even if it's a file.
func AppendExtension(name string) string {
	// to fit pattern
	if strings.HasSuffix(name, ".htm") {
		return name + "l"
	}

	if strings.HasSuffix(name, ".html") {
		return name
	}

	return name + ".html"
}
