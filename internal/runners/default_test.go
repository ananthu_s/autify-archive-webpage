package runners

import (
	"archive-webpage/test"
	"github.com/urfave/cli/v2"
	"os"
	"testing"
)

func TestDefault(t *testing.T) {
	tempDir := test.GetTempDirectory()

	tests := []struct {
		name    string
		args    []string
		wantErr bool
	}{
		{
			name:    "should return error when no urls are passed",
			args:    []string{},
			wantErr: true,
		},
		{
			name:    "should parse successfully",
			args:    []string{"https://www.google.com"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := &cli.App{
				Action: Default,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:    "archive-folder",
						Aliases: []string{"a"},
						Value:   tempDir,
						Usage:   "Path to where the archives should be stored",
					},
				},
			}

			if err := app.Run(append([]string{"runPath"}, tt.args...)); (err != nil) != tt.wantErr {
				t.Errorf("Default() error = %v, wantErr %v", err, tt.wantErr)
			}

			t.Cleanup(func() {
				_ = os.RemoveAll(tempDir)
			})
		})
	}
}
