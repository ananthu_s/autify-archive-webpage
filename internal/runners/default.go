package runners

import (
	"archive-webpage/pkg/localise"
	"archive-webpage/pkg/parse"
	"errors"
	"github.com/urfave/cli/v2"
	"log"
	"net/url"
	"sync"
)

func Default(context *cli.Context) (err error) {
	// quits runners if no argument is passed
	if context.Args().Len() < 1 {
		return errors.New("please enter at least one url")
	}

	parsedUrls, failedUrls := parse.Urls(context.Args().Slice()...)
	parsedUrlsLength := len(parsedUrls)

	wg := sync.WaitGroup{}
	wg.Add(parsedUrlsLength)
	assetLocaliseChan := make(chan *localise.SelectionPayload)

	go func() {
		for {
			select {
			case payload := <-assetLocaliseChan:
				go localise.Selected(payload)
			}
		}
	}()

	for _, parsedUrl := range parsedUrls {
		go func(url *url.URL) {
			defer wg.Done()

			if innerErr := localise.Url(url, assetLocaliseChan); innerErr != nil {
				log.Println(innerErr.Error())
			}
		}(parsedUrl)
	}

	wg.Wait()
	close(assetLocaliseChan)

	for _, failedUrl := range failedUrls {
		log.Println("failed to parse:", failedUrl)
	}

	return nil
}
