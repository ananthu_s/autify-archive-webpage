package test

import (
	"github.com/tidwall/buntdb"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

var (
	_, b, _, _    = runtime.Caller(0)
	runDirectory  = filepath.Dir(b)
	testDirectory = filepath.Join(runDirectory, "..", "test")
)

func GetTempDirectory() string {
	temp, err := os.MkdirTemp("", "temp-")
	if err != nil {
		log.Fatal("temp directory creation error")
	}
	return temp
}

func GetTempFilePath(fileName string) string {
	return path.Join(GetTempDirectory(), fileName)
}

func GetTestDirectory(paths ...string) string {
	return path.Join(append([]string{testDirectory}, paths...)...)
}

func GetTempDb() (db *buntdb.DB, err error) {
	return buntdb.Open(":memory:")
}
