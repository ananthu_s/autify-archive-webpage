package parse

import (
	"net/url"
	"reflect"
	"testing"
)

func TestUrls(t *testing.T) {
	google, _ := url.Parse("https://www.google.com")

	type args struct {
		stringUrls []string
	}

	tests := []struct {
		name           string
		args           args
		wantParsedUrls []*url.URL
		wantFailedUrls []string
	}{
		{
			name:           "Should return empty if no string urls are provided",
			args:           args{stringUrls: []string{}},
			wantParsedUrls: nil,
			wantFailedUrls: nil,
		},
		{
			name:           "Should return URL struct for valid url",
			args:           args{stringUrls: []string{"https://www.google.com"}},
			wantParsedUrls: []*url.URL{google},
			wantFailedUrls: nil,
		},
		{
			name:           "Should return URL struct for valid url and failed url",
			args:           args{stringUrls: []string{"https://www.google.com", "failedurl"}},
			wantParsedUrls: []*url.URL{google},
			wantFailedUrls: []string{"failedurl"},
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotParsedUrls, gotFailedUrls := Urls(tt.args.stringUrls...)
			if !reflect.DeepEqual(gotParsedUrls, tt.wantParsedUrls) {
				t.Errorf("Urls() gotParsedUrls = %v, want %v", gotParsedUrls, tt.wantParsedUrls)
			}
			if !reflect.DeepEqual(gotFailedUrls, tt.wantFailedUrls) {
				t.Errorf("Urls() gotFailedUrls = %v, want %v", gotFailedUrls, tt.wantFailedUrls)
			}
		})
	}
}
