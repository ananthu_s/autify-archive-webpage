package name

import (
	"net/url"
	"path"
	"strings"
)

// FromUrl
// Creates `file` Name and `folder` names (if nested route) from a URL.
func FromUrl(link *url.URL) (fileName string, folder string) {
	folder = ""
	// Remove https prefix (if it exists).
	fileName = strings.TrimPrefix(link.String(), link.Scheme)
	fileName = strings.TrimPrefix(fileName, "://")

	if link.Path != "" {
		// If path exists then the filename should be the path names, and it would be nested.
		folder = link.Host
		fileName = strings.TrimPrefix(fileName, link.Host)

		// to remove the first slash
		fileName = strings.TrimPrefix(fileName, "/")
		paths := strings.Split(fileName, "/")

		// `fileName` would be the last `path` part.
		// `folder` would be the paths leading upto the last part of the url.
		fileName = paths[len(paths)-1]
		folder = path.Join(append([]string{folder}, paths[:len(paths)-1]...)...)
	}
	return
}
