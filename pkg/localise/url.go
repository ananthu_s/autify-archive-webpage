package localise

import (
	"archive-webpage/config"
	"archive-webpage/pkg/common"
	"archive-webpage/pkg/db"
	"archive-webpage/pkg/download"
	"archive-webpage/pkg/name"
	"github.com/PuerkitoBio/goquery"
	"github.com/tidwall/buntdb"
	"log"
	"net/url"
	"os"
	"path"
	"strconv"
	"sync"
	"time"
)

// Url
// Takes a `url` and downloads it to the `archiveFolderPath`.
// After successful download send the `url` to `assetLocaliseChan` for processing each valid element (selection).
func Url(pageUrl *url.URL, assetLocaliseChan chan *SelectionPayload) error {
	timestamp := time.Now()

	downloadFolderPath := common.RelativeToAbsolutePath(config.DownloadFolderPath)
	archiveFolderPath := common.RelativeToAbsolutePath(config.ArchiveFolderPath)

	query, err := url.JoinPath(pageUrl.Path, pageUrl.RawQuery)
	if err != nil {
		return err
	}

	queryEscaped := url.QueryEscape(query)
	fullName := name.AppendExtension(pageUrl.Host + queryEscaped)

	htmlPath := path.Join(downloadFolderPath, fullName)

	if err = os.MkdirAll(path.Dir(htmlPath), os.ModePerm); err != nil {
		return err
	}

	err = download.ToFile(pageUrl.String(), htmlPath)

	if err != nil {
		return err
	}

	document, innerErr := parseToDocument(htmlPath)
	if innerErr != nil {
		return innerErr
	}

	selectionWg := sync.WaitGroup{}
	pageUrlCache := sync.Map{}
	var pageLinksCount uint = 0
	var pageImagesCount uint = 0

	sendSelectionToChannel := func(attribute string, isImage bool) func(int, *goquery.Selection) {
		return func(index int, selection *goquery.Selection) {
			selectionWg.Add(selection.Length())
			assetLocaliseChan <- &SelectionPayload{
				selected:     selection,
				pageUrl:      pageUrl,
				folderPath:   archiveFolderPath,
				timestamp:    timestamp,
				attribute:    attribute,
				document:     document,
				pageWg:       &selectionWg,
				pageUrlCache: &pageUrlCache,
				isImage:      isImage,
				linksCount:   &pageLinksCount,
				imagesCount:  &pageImagesCount,
			}
		}
	}

	// Add more here:
	document.Find("img[src]").Each(sendSelectionToChannel("src", true))
	document.Find("script[src]").Each(sendSelectionToChannel("src", false))
	document.Find("link[href]").Each(sendSelectionToChannel("href", false))

	selectionWg.Wait()

	archiveHtmlPath := path.Join(archiveFolderPath, pageUrl.Host, pageUrl.Path, pageUrl.RawQuery, "index.html")

	archiveHtmlDestination, err := os.Create(archiveHtmlPath)

	if err != nil {
		return err
	}

	defer func(archiveHtmlDestination *os.File) {
		err = archiveHtmlDestination.Close()
		if err != nil {
			log.Printf("failed to close file `archiveHtmlDestination`")
		}
	}(archiveHtmlDestination)

	documentHtml, err := document.Html()
	if err != nil {
		return err
	}

	_, err = archiveHtmlDestination.WriteString(documentHtml)

	if err != nil {
		return err
	}

	counterDb, err := buntdb.Open(path.Join(archiveFolderPath, "db"))

	if err != nil {
		return err
	}

	err = db.UpdateUrl(counterDb, pageUrl, strconv.Itoa(int(pageLinksCount)), strconv.Itoa(int(pageImagesCount)), timestamp)

	if err != nil {
		return err
	}

	return nil
}
