package localise

import (
	"github.com/PuerkitoBio/goquery"
	"net/url"
	"sync"
	"time"
)

type SelectionPayload struct {
	document  *goquery.Document
	selected  *goquery.Selection
	pageUrl   *url.URL
	timestamp time.Time
	// folderPath
	// Path to where the assets are to be downloaded.
	folderPath string
	attribute  string
	// pageWg, to wait until a page has been fully completed.
	// After which index.html file will be created with relative urls.
	pageWg       *sync.WaitGroup
	pageUrlCache *sync.Map
	isImage      bool
	linksCount   *uint
	imagesCount  *uint
}
