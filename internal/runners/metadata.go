package runners

import (
	"archive-webpage/config"
	"archive-webpage/pkg/common"
	"archive-webpage/pkg/db"
	"errors"
	"fmt"
	"github.com/tidwall/buntdb"
	"github.com/urfave/cli/v2"
	"path"
)

func Metadata(context *cli.Context) error {
	if context.Args().Len() < 1 {
		return errors.New("please enter at least one url")
	}

	archiveFolderPath := common.RelativeToAbsolutePath(config.ArchiveFolderPath)
	counterDb, err := buntdb.Open(path.Join(archiveFolderPath, "db"))

	if err != nil {
		return err
	}

	for _, src := range context.Args().Slice() {
		numLinks, imageLinks, lastFetch, innerErr := db.GetUrlMetadata(counterDb, src)

		fmt.Printf("site: %s\n", src)
		fmt.Printf("num_links: %s\n", numLinks)
		fmt.Printf("images: %s\n", imageLinks)
		fmt.Printf("lastFetch: %s\n\n", lastFetch)

		if innerErr != nil {
			return innerErr
		}

	}

	return nil
}
