# Introduction

> Expect `/data/archive` for the archives and `/data/download` for the raw html files when using the example scripts.

## Main

- The language of choice is `go`, as since downloading different assets a language with multithreading (goroutines)
  would've been benifitial.
- Architecture follows a simple `go` `channel` which listens to a `payload` which contains `url` and other information
  which helps spin up a `go` routine for each. This enables the program to very quickly parse and download assets
  without the need of much performance optimisation.
- In these channels each `element` that matches the pattern (example: `/pkg/localise/url.go` line number 79) will
  undergo localising, i.e. the relative urls and absolute urls will be downloaded and the resulting `index.html` will be
  modified.
- The modified `html` file lives withing the `archive` folder, which can be changed by using the `-a` flag.
- The original `html` files are stored in `root` of the program which can be changed by using the `-d` flag.

## download

- The files downloaded are sanitised so all can live in a single directory.

## archive

- The archive folder are partitioned based on the url, hence `autify.com/webinars` would be inside `autify.com` which
  would house `webinars` route.
- This was done to preserve the one location for all.
- The html file will be renamed to `index.html` so all archives follow the same format.
-

## Metadata

- By calling the program with the `metadata` sub command, followed by the urls; the program will out the number of
  links, number of images and the timestamp of when it was archived.
- `-a` and `-d` flags still hold for the application.

# Features

- `-a` and `-d` flags, provide a way to handle multiple sessions quite easily.
- The paralleled goroutines help improve the speed and scalability of the app.
- The app checks for each url (using hashing), if multiple same links are present and only downloads them once, using
  go's inbuilt `sync.Map` which allows multiple threads to use it simultaneously without manually dealing with `mutex`.

# Things to be done

- Unit tests for basic packages has to be improved (lack of time to unit test all).
- The database (file based) tests are missing.

---

# Project Structure

> The app uses the cli framework (`urfave/cli`) to manage the and subcommands.
> `goquery` is used to modify `html` files.

## config

> This is where the cli arugment configuration is stored. Namely, the `downlaoad` and `archive` folders.
---

## internal

> This folder contains all the application based files. (following go convention as to not allow these to be imported by
> other packages.)

## pkg

> Houses most of the codebase.

## test

> Preconfigured files and examples are stored in this folder.

# Build scripts

## Docker build script

```shell
docker build . -t archive-webpage:latest
```

# Run scripts

## Docker runner script

Since inside the docker container more configuration is needed to make it run like a binary, `-a` and `-b` are necessary
while running as a docker container.

The example below is an example of doing so.
This pattern allows for the separation of `task-01` which is to download html file and `task-03` which is to create an
archive by localising all the assets.
`/data/archive` will house all downloaded `assets` as well as a `db` file, which store the metadata information.

```shell
docker run -it --rm --volume="$(pwd)/data:/data" archive-webpage:latest \
-a "/data/archive" \
-d "/data/download" \
http://www.google.com \
http://www.autify.com \
"https://autify.com/webinars?should_join=false"
```

## Docker metadata script

```shell
docker run -it --rm --volume="$(pwd)/data:/data" archive-webpage:latest \
metadata \
-a "/data/archive" \
-d "/data/download" \
http://www.google.com \
http://www.autify.com \
"https://autify.com/webinars?should_join=false"
```

---

# Docker Compose

- Contains example services.
- They are not to be run simultaneously as since `metadata` needs `runner` to finish first before querying data.
- 