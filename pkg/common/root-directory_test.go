package common

import (
	"os"
	"path"
	"testing"
)

// Cannot test without configuring other parameters
func TestRootDir(t *testing.T) {
	workingDirectory, _ := os.Executable()

	tests := []struct {
		name string
		want string
	}{
		{
			name: "should return the root directory",
			want: path.Dir(workingDirectory),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RootDir(); got != tt.want {
				t.Errorf("RootDir() = %v, want %v", got, tt.want)
			}
		})
	}
}
